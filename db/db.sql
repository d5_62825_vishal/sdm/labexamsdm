ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'manager';
flush privileges;

DROP DATABASE IF EXISTS employeedb;
CREATE DATABASE employeedb;
USE employeedb;
CREATE TABLE employee( emp_id INT PRIMARY KEY AUTO_INCREMENT,emp_Name VARCHAR(40), emp_Address VARCHAR(40),emp_Email VARCHAR(40),emp_MobileNo VARCHAR(40),emp_DOB DATE, emp_JoiningDate DATE);
INSERT INTO employee ( emp_Name,emp_Address,emp_Email,emp_MobileNo,emp_DOB,emp_JoiningDate) VALUES ('Vishal Shinde','Pune', 'shinde@gmail.com','7373637363',date '1994-12-19',date '2017-17-07');

