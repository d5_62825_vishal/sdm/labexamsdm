const express =require('express')
const db =require ('../db.js')
const utils =require('../utils')
const router =express.Router();

router.get('/',(request,response)=>{
    const {emp_name}=request.query;
    const query=`SELECT * FROM employee WHERE emp_Name='${emp_name}';`

    db.query(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});

router.post('/addemp',(request,response)=>{
    const {emp_Name, emp_Address ,emp_Email,emp_MobileNo,emp_DOB, emp_JoiningDate}=request.body;
    const query=`INSERT INTO employee (emp_Name, emp_Address ,emp_Email,emp_MobileNo,emp_DOB, emp_JoiningDate) VALUES('${emp_Name}', '${emp_Address}' ,'${emp_Email}','${emp_MobileNo}',date '${emp_DOB}', date ' ${emp_JoiningDate}');`

    db.query(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});

router.put('/update/:emp_Name',(request,response)=>{
    const {emp_Name}=request.params;

    const {emp_Address ,emp_Email,emp_MobileNo}=request.body;
    const query=`UPDATE employee SET  emp_Address='${emp_Address}' ,emp_Email='${emp_Email}',emp_MobileNo='${emp_MobileNo}' WHERE emp_name='${emp_Name}';`

    db.query(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});

router.delete('/:emp_Name',(request,response)=>{
    const {emp_Name}=request.params;
    const query=`DELETE FROM employee WHERE emp_Name='${emp_Name}';`

    db.query(query,(error,result)=>{
        response.send(utils.createResult(error,result));
    });
});
module.exports=router