const express =require('express')
const cors=require ('cors')
const app=express()
app.use(cors('*'))
app.use(express.json())

const employeerouter=require('./routes/employee')
app.use('/employee',employeerouter)

app.listen(4000,'0.0.0.0',()=>{
    console.log('server listening on port 4000')
})
